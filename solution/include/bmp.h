#ifndef IMAGE_TRANSFORMER_BMP_H_
#define IMAGE_TRANSFORMER_BMP_H_

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header init_bmp_header(uint32_t height, uint32_t width);

enum read_status from_bmp(FILE *in, struct image *image);
enum write_status to_bmp(FILE *out, struct image *image);

#endif // IMAGE_TRANSFORMER_BMP_H_
