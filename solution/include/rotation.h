#ifndef IMAGE_TRANSFORMER_POVOROT_H
#define IMAGE_TRANSFORMER_POVOROT_H

#include "image.h"

struct image rotated(struct image const source);

#endif // IMAGE_TRANSFORMER_POVOROT_H
