#ifndef IMAGE_TRANSFORMER_IMAGE_H_
#define IMAGE_TRANSFORMER_IMAGE_H_

#include <stdint.h>
#include <stdio.h>

enum read_status { READ_OK = 0, READ_FILE_ERROR };

enum write_status { WRITE_OK = 0, WRITE_FILE_ERROR };

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

uint32_t get_padding(uint32_t width);

uint32_t get_pixel_array_size(uint32_t width, uint32_t height,
                              uint32_t padding);

enum write_status write_image(struct image *image, uint32_t padding, FILE *out);

enum read_status read_image(struct image *image, uint32_t padding, FILE *in);

#endif // IMAGE_TRANSFORMER_IMAGE_H_
