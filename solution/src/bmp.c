#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header init_bmp_header(uint32_t height, uint32_t width) {
    return (struct bmp_header){
        .bfType = 0x4D42,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header bmp_header = {0};

    fread(&bmp_header, sizeof(struct bmp_header), 1, in);
    image->width = bmp_header.biWidth;
    image->height = bmp_header.biHeight;

    uint32_t padding = get_padding(image->width);
    image->data = malloc(sizeof(struct pixel) * image->width * image->height);

    return read_image(image, padding, in);
}

enum write_status to_bmp(FILE *out, struct image *image) {
    uint32_t padding = get_padding(image->width);

    struct bmp_header bmp_header = init_bmp_header(image->height, image->width);
    fwrite(&bmp_header, sizeof(struct bmp_header), 1, out);

    return write_image(image, padding, out);
}
