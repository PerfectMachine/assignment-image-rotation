#include "bmp.h"
#include "image.h"
#include "rotation.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("image-tranformer [source] [out]\n");
        return 1;
    }

    FILE *source_file = fopen(argv[1], "rb");

    if (source_file == NULL) {
        printf("Can't open file\n");
        return 1;
    }
    struct image source_image = {0};
    from_bmp(source_file, &source_image);
    fclose(source_file);

    struct image rotated_image = rotated(source_image);
    free(source_image.data);

    FILE *out_file = fopen(argv[2], "wb");
    if (out_file == NULL) {
        printf("Can't write file\n");
        return 1;
    }
    to_bmp(out_file, &rotated_image);
    free(rotated_image.data);
    fclose(out_file);

    return 0;
}
