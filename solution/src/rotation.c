#include "rotation.h"
#include <stdlib.h>

struct image rotated(struct image const source) {
    struct pixel *pixels = source.data;
    struct pixel *rotation =
        malloc(sizeof(struct pixel) * source.height * source.width);

    for (size_t i = 0; i < source.height; i++)
        for (size_t j = 0; j < source.width; j++) {
            rotation[source.height - i - 1 + j * source.height] =
                pixels[j + i * source.width];
        }

    struct image rotated_image;
    rotated_image.height = source.width;
    rotated_image.width = source.height;
    rotated_image.data = rotation;
    return rotated_image;
}
