#include "image.h"
#include <stdio.h>

uint32_t get_padding(uint32_t width) { return (4 - (3 * width) % 4) % 4; }

uint32_t get_pixel_array_size(uint32_t width, uint32_t height,
                              uint32_t padding) {
    return (width * sizeof(struct pixel) + padding) * height;
}

enum read_status read_image(struct image *image, uint32_t padding, FILE *in) {
    for (size_t i = 0; i < image->height; i++) {
        fread(i * image->width + image->data, sizeof(struct pixel),
              image->width, in);
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status write_image(struct image *image, uint32_t padding,
                              FILE *out) {
    for (size_t i = 0; i < image->height; i++) {
        fwrite(i * image->width + image->data, sizeof(struct pixel),
               image->width, out);
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}
